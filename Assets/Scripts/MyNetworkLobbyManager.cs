﻿using UnityEngine;
using UnityEngine.Networking;

using System.Collections;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.Networking.Match;

public class MyNetworkLobbyManager : NetworkLobbyManager {



    void OnGUI()
    {
        if (!showLobbyGUI)
            return;

        string loadedSceneName = SceneManager.GetSceneAt(0).name;
              
        Rect backgroundRec = new Rect(90, 180, 500, 150);
        GUI.Box(backgroundRec, "Players:");

        if (NetworkClient.active)
        {
            Rect addRec = new Rect(100, 300, 120, 20);
            if (GUI.Button(addRec, "Add Player"))
            {
                TryToAddPlayer();
            }
            Rect leaveRec = new Rect(400, 300, 120, 20);
            if (GUI.Button(leaveRec, "Leave Lobby"))
            {
                LeaveLobby();
            }
        }
    }
    private ulong _netId;
    public override void OnMatchCreate(CreateMatchResponse matchInfo)
    {
        base.OnMatchCreate(matchInfo);
        _netId = (ulong) matchInfo.networkId;
        Debug.Log("Created Networkid: " + _netId);
    }

    void LeaveLobby()
    {
        Debug.Log("leaving lobby");
        matchMaker.DestroyMatch((UnityEngine.Networking.Types.NetworkID)_netId, DestroyCallback);
    }

    private void DestroyCallback(BasicResponse response)
    {
        Debug.Log("response: " + response);
    }
}
